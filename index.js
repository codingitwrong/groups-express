const express = require('express')
const http = require('http')
const cors = require('cors')
const {parse} = require('url')
const configureRoutes = require('./routes')
const {configureWs} = require('./ws')

const app = express()
app.use(cors())
app.use(express.json()) // json body parsing

configureRoutes(app)

const port = process.env.PORT || 3000
const httpServer = http.createServer(app)
httpServer.listen(port, () => {
  console.log(`Express ready at http://localhost:${port}`)
})

const WebSocket = require('ws')
const wss = new WebSocket.Server({noServer: true})
configureWs(wss)

httpServer.on('upgrade', (request, socket, head) => {
  const {pathname} = parse(request.url)
  if (pathname === '/ws') {
    wss.handleUpgrade(request, socket, head, ws => {
      wss.emit('connection', ws, request)
    })
  }
})
