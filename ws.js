let wss

function configureWs(newWss) {
  wss = newWss
  wss.on('connection', function (ws) {
    console.log('client connected')
    ws.on('close', function close() {
      console.log('client disconnected')
    })
  })
}

function notifyAboutPost(post) {
  wss.clients.forEach(client => {
    client.send(JSON.stringify(post))
  })
}

module.exports = {configureWs, notifyAboutPost}
