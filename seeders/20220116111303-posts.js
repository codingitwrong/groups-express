'use strict'
const {times} = require('lodash')

module.exports = {
  async up(queryInterface, Sequelize) {
    const groupIds = times(10, i => i + 1)
    const posts = groupIds.flatMap(groupId =>
      times(10, postIndex => ({
        parentType: 'Group',
        parentId: groupId,
        message: `Message ${postIndex + 1} for group ${groupId}`,
      }))
    )
    console.log(posts)
    await queryInterface.bulkInsert('Posts', posts)
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Posts', null, {})
  },
}
