'use strict'
const {times} = require('lodash')

module.exports = {
  async up(queryInterface, Sequelize) {
    const groups = times(10, i => ({
      id: i + 1,
      name: `Group ${i + 1}`,
      description: `This is sample group ${i + 1}.`,
    }))
    console.log(groups)
    await queryInterface.bulkInsert('Groups', groups)
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Groups', null, {})
  },
}
