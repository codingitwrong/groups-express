module.exports = {
  arrowParens: 'avoid',
  bracketSpacing: false,
  semi: false,
  singleQuote: true,
  trailingComma: 'es5',
}
