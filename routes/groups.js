const express = require('express')
const {Post} = require('../models')
const {notifyAboutPost} = require('../ws')

const groupsRouter = express.Router()
groupsRouter.get('/:groupId/posts', async (req, res) => {
  const {groupId} = req.params
  const posts = await Post.findAll({
    where: {parentType: 'Group', parentId: groupId},
  })
  res.send(posts)
})

groupsRouter.post('/:groupId/posts', async (req, res) => {
  const {groupId} = req.params
  const {message} = req.body

  try {
    const post = await Post.create({
      parentType: 'Group',
      parentId: groupId,
      message,
    })
    notifyAboutPost(post)
    res.send(post)
  } catch (e) {
    res.status(422)
    res.send(e.message)
  }
})

module.exports = groupsRouter
