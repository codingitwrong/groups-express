const groupsRouter = require('./groups')

function configureRoutes(app) {
  app.get('/', (req, res) => {
    res.send('Hello, world!')
  })
  app.use('/groups', groupsRouter)
}

module.exports = configureRoutes
