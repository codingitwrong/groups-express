# groups-express

An open-source app for groups.

## Requirements

- Node (LTS 16+ recommended)
- Yarn
- Postgres

## Installation

```bash
$ yarn install
$ createdb groups_development
$ createdb groups_test
$ yarn seq db:migrate
$ yarn seq db:seed:all
```

## Development

```bash
$ yarn start
```

## License

MIT
